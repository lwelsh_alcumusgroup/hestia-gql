const express = require('express');
const cors = require('cors');
const express_graphql = require('express-graphql');
const { buildSchema } = require('graphql');
const nano = require('nano');
const _ = require('lodash');

const {
    couchPort,
    dbName,
    SERVER_ADDRESS,
    COUCH_PASSWORD,
    protocol,
    serverPort,
    COUCH_USER
  } = require("./config.json");

const couchServerUrl = `https://${COUCH_USER}:${COUCH_PASSWORD}@${SERVER_ADDRESS}`;

const couchConnection = nano(couchServerUrl);

const createDbHandles = (namespace, user) => {
    return Promise.all([
        couchConnection.db.use(`${namespace}-system`),
        couchConnection.db.use(`${namespace}-${user}`)
    ]);
}

const schema = buildSchema(`
    type Query {
        message: String,
        status: String,
        documentCheckin(documentId: String, namespace: String, user: String): Message
        documentCheckout(documentId: String, namespace: String, user: String): Document
    },
    type Message {
        status: String,
        message: String
    },
    type Document {
        _id: String,
        _rev: String,
        originRevision: String,
        content: String
    }
`);

const status = () => {
    return "Connected";
};

const copyUserDocument = (document) => {
    const _rev = document.originRevision;
    return Object.assign({ _rev }, _.omit(document, ['_rev', 'originRevision', 'documentId', 'pending']));
};

const copySystemDocument = (document) => {
    const originRevision = document._rev;
    return Object.assign({ originRevision }, _.omit(document, ['_rev']));
};

const deleteDocument = (userCouch, documentId, rev) => {
    console.log('deleting', documentId, rev);
    return userCouch.destroy(documentId, rev).then(console.log('deleted')).catch((err) => console.error(err));
};

const documentCheckin = async(payload) => {
    const { documentId, namespace, user } = payload;

    createDbHandles(namespace, user).then(([systemCouch, userCouch]) => {
        // need to make copy and put into system then delete from user
        return userCouch.get(documentId).then((doc) => {
            systemCouch.insert(copyUserDocument(doc))
            .then(deleteDocument(userCouch, doc._id, doc._rev))
            .catch((err) => console.error(err));
        }).catch((err) => console.error(err));;
    })
    .catch((err) => console.error(err));
}

const documentCheckout = async(payload) => {
    const { documentId, namespace, user } = payload;

    createDbHandles(namespace, user).then(([systemCouch, userCouch]) => {
        // need to make copy and put into system then delete from user
        return systemCouch.get(documentId).then((doc) => {
            userCouch.insert(copySystemDocument(doc))
            .catch((err) => console.error(err));
        }).catch((err) => console.error(err));;
    })
    .catch((err) => console.error(err));
}

const root = {
    message: () => 'Hello World',
    status: status ,
    documentCheckin: documentCheckin,
    documentCheckout: documentCheckout
};

const app = express();

app.use(cors());

app.use('/graphyql', express_graphql({
    schema,
    rootValue: root,
    graphiql: true
}));

app.listen(4000, () => console.log('GraphQL server running on 4000'));